package random

import (
	"math/rand"
	"time"

	"gitlab.com/ioto/common/gstuff/constant"
)

var seededRand *rand.Rand = rand.New(
	rand.NewSource(time.Now().UnixNano()))

func StringWithCharset(length int, charset string) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

func String(length int) string {
	return StringWithCharset(length, constant.Charset)
}

type UniqueRand struct {
	generated map[int]bool //keeps track of
	rng       *rand.Rand   //underlying random number generator
	scope     int          //scope of number to be generated
}

//Generating unique rand less than N
//If N is less or equal to 0, the scope will be unlimited
//If N is greater than 0, it will generate (-scope, +scope)
//If no more unique number can be generated, it will return -1 forwards
func NewUniqueRand(N int) *UniqueRand {
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)
	return &UniqueRand{
		generated: map[int]bool{},
		rng:       r1,
		scope:     N,
	}
}

func (u *UniqueRand) Int() int {
	if u.scope > 0 && len(u.generated) >= u.scope {
		return -1
	}
	for {
		var i int
		if u.scope > 0 {
			i = u.rng.Int() % u.scope
		} else {
			i = u.rng.Int()
		}
		if !u.generated[i] {
			u.generated[i] = true
			return i
		}
	}
}

func RandomInList(in []int) (pick int) {
	randomIndex := rand.Intn(len(in))
	pick = in[randomIndex]
	return
}

func RandomList(in []int) (out []int) {
	Length := len(in)
	for i := 0; i < Length; i++ {
		pickNum := RandomInList(in)
		out = append(out, pickNum)
		in = removeItemInList(in, pickNum)
	}
	return
}

func removeItemInList(Items []int, Item int) (NewList []int) {
	for _, ele := range Items {
		if ele != Item {
			NewList = append(NewList, ele)
		}
	}
	return
}
