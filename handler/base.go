package handler

import (
	"net/http"
	"regexp"

	"log"

	"github.com/labstack/echo/v4"
	"gitlab.com/ioto/common/config"
	validator "gopkg.in/go-playground/validator.v9"
)

var cfg = config.GetConfig()

// NewValidator ..
func NewValidator() *MyValidator {
	validator := validator.New()
	validator.RegisterValidation("phone", IsValidPhone)
	validator.RegisterValidation("enginemode", IsValidEngineMode)
	validator.RegisterValidation("cartype", IsValidCarType)
	validator.RegisterValidation("enginefuel", IsValidEngineFuel)
	validator.RegisterValidation("drivesystem", IsValidDriveSystem)
	return &MyValidator{validator}
}

// MyValidator ..
type MyValidator struct {
	validator *validator.Validate
}

// Validate ..
func (cv *MyValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}

func IsValidPhone(fl validator.FieldLevel) bool {
	PhoneRegex := regexp.MustCompile(`^\+84[0-9]{9}`)
	return PhoneRegex.MatchString(fl.Field().String())
}

func IsValidEngineMode(fl validator.FieldLevel) bool {
	//defined gender
	CheckCondition := map[string]bool{
		"at": true,
		"mt": true,
	}
	return CheckCondition[fl.Field().String()]
}

func IsValidCarType(fl validator.FieldLevel) bool {
	//defined gender
	CheckCondition := map[string]bool{
		"sedan":     true,
		"suv":       true,
		"cuv":       true,
		"hatchback": true,
		"pickup":    true,
		"mpv":       true,
	}
	return CheckCondition[fl.Field().String()]
}

func IsValidEngineFuel(fl validator.FieldLevel) bool {
	//defined gender
	CheckCondition := map[string]bool{
		"xang":   true,
		"oil":    true,
		"dien":   true,
		"hybrid": true,
	}
	return CheckCondition[fl.Field().String()]
}

func IsValidDriveSystem(fl validator.FieldLevel) bool {
	//defined gender
	CheckCondition := map[string]bool{
		"2wd": true,
		"4wd": true,
	}
	return CheckCondition[fl.Field().String()]
}

// GetRequestID ..
func GetRequestID(c echo.Context) (requestID string) {
	if c.Get("reqID") != nil {
		requestID = c.Get("reqID").(string)
	}
	return
}

// Health func
func Health(c echo.Context) (err error) {
	return c.JSON(Success(nil))
}

// ResponseContent struct
type ResponseContent struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

// Error func
func Error(err error, c echo.Context) {
	code := http.StatusBadRequest
	msg := http.StatusText(code)

	if httpError, ok := err.(*echo.HTTPError); ok {
		code = httpError.Code
		msg = httpError.Message.(string)
	}

	if cfg.Debug {
		msg = err.Error()
	}

	log.Println(err)

	if !c.Response().Committed {
		if c.Request().Method == echo.HEAD {
			c.NoContent(code)
		} else {
			c.JSON(code, &ResponseContent{Code: code, Message: msg})
		}
	}
}

// Success func
func Success(data interface{}) (int, ResponseContent) {
	return http.StatusOK, ResponseContent{
		Code:    http.StatusOK,
		Message: "Success",
		Data:    data,
	}
}

// NotFound func
func NotFound(message string) (int, ResponseContent) {
	return http.StatusOK, ResponseContent{
		Code:    http.StatusNotFound,
		Message: message,
	}
}
