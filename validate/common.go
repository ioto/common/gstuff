package validate

import (
	"mime/multipart"
	"net/http"
	"regexp"

	"gitlab.com/ioto/common/gstuff/constant"
)

type vYeah1Validator struct{}

var Yeah1Validator vYeah1Validator

// Valid UploadFile
func (vYeah1Validator) UploadFile(fileUpload *multipart.FileHeader, AllowTypes []string) (fileType string, isOK bool) {
	f, err := fileUpload.Open()
	if err != nil {
		return
	}
	defer f.Close()

	buffer := make([]byte, 512)

	_, err = f.Read(buffer)
	if err != nil {
		return
	}

	// Use the net/http package's handy DectectContentType function. Always returns a valid
	// content-type by returning "application/octet-stream" if no others seemed to match.
	contentType := http.DetectContentType(buffer)

	for _, ele := range AllowTypes {
		if contentType == ele {
			return constant.MediaTypes[contentType], true
		}
	}
	return
}

// Validate UserName
func (vYeah1Validator) UserName(UserName string) bool {
	usernameConvention := "^[a-zA-Z0-9]*[-_.]?[a-zA-Z0-9]*$"
	re, _ := regexp.Compile(usernameConvention)
	if re.MatchString(UserName) {
		return true
	}
	return false
}

// Validate Email
func (vYeah1Validator) Email(email string) bool {
	Re := regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`)
	return Re.MatchString(email)
}

// Validate Phone
func (vYeah1Validator) Phone(phone string) bool {
	Re := regexp.MustCompile(`(0[98753])+([0-9]{8})\b`)
	return Re.MatchString(phone)
}
