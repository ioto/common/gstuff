package route

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/ioto/common/gstuff/handler"
	gmiddleware "gitlab.com/ioto/common/gstuff/middleware"
)

func baseRoute(e *echo.Echo) *echo.Group {
	return e.Group("", gmiddleware.LogBody)
}

// APIRoute ..
func APIRoute(e *echo.Echo) *echo.Group {
	base := baseRoute(e)
	apiRoute := base.Group("/api")
	apiRoute.Any("/health", handler.Health)
	return apiRoute
}

// PublicAPIRoute ..
func PublicAPIRoute(e *echo.Echo) *echo.Group {
	base := baseRoute(e)
	publicApiRoute := base.Group("/public-api")
	publicApiRoute.Any("/health", handler.Health)
	return publicApiRoute
}
