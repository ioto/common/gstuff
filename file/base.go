package file

import (
	"fmt"
	"strings"
	"time"
	"github.com/google/uuid"
)

var AllowVideoTypes = []string{".mp4", ".avi", ".webm", ".flv", ".swf", ".mpg", ".mp2", ".mpeg", ".mpe", ".mpv", ".ogg", ".m4p", ".m4v", ".wmv", ".mov", ".qt", ".avchd"}

var AllowImageTypes = []string{".jpg", ".jpeg", ".png", ".gif"}


// get file type
func GetFileType(Filename string) (FileType string) {
	parse := strings.Split(Filename, ".")
	return fmt.Sprintf(".%s", parse[len(parse)-1])
}

func CreatePath(OldFileName, Path string) string {
	NowDate := time.Now().Format("2006-01-02")
	NowDate = strings.ReplaceAll(NowDate, "-", "")
	return fmt.Sprintf("%s%s/%s%s", Path, NowDate, uuid.New().String(), GetFileType(OldFileName))
}

