package file

import (
	"github.com/google/uuid"
	"io"
	"mime/multipart"
	"os"
	"strings"
	"time"
)

func SaveWithImageFile(attachment *multipart.FileHeader, ImagePath string) (imagePath string, err error) {

	DateOfSale := time.Now().Format("2006-01-02")
	DateOfSale = strings.ReplaceAll(DateOfSale, "-", "")

	DirPath := ImagePath + DateOfSale
	if _, err := os.Stat(DirPath); os.IsNotExist(err) {
		_ = os.Mkdir(DirPath, os.ModePerm)
	}

	fileType := ".jpg"
	filename := strings.ToLower(attachment.Filename)
	for _, file_type := range AllowImageTypes {
		if strings.HasSuffix(filename, file_type) {
			fileType = file_type
			break
		}
	}

	imageName := uuid.New().String() + fileType

	var file *os.File
	file, err = os.Create(DirPath + "/" + imageName)
	if err != nil {
		return
	}
	defer file.Close()

	imageFile, _ := attachment.Open()

	// Use io.Copy to just dump the response body to the file. This supports huge files
	_, err = io.Copy(file, imageFile)
	//err = png.Encode(file, attachment)
	if err != nil {
		return
	}

	imagePath = DateOfSale + "/" + imageName
	return
}
