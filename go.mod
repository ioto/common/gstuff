module gitlab.com/ioto/common/gstuff

go 1.12

require (
	github.com/aws/aws-sdk-go v1.36.30 // indirect
	github.com/denisenkom/go-mssqldb v0.9.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fatih/structs v1.1.0 // indirect
	github.com/gemnasium/logrus-graylog-hook/v3 v3.0.2 // indirect
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-redis/redis v6.15.9+incompatible // indirect
	github.com/go-xorm/xorm v0.7.9 // indirect
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.1.17
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/lib/pq v1.9.0 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible // indirect
	github.com/spf13/viper v1.7.1 // indirect
	github.com/streadway/amqp v1.0.0 // indirect
	gitlab.com/ioto/common/adapter v0.0.0-20201127045327-3f3d78dfdcf4 // indirect
	gitlab.com/ioto/common/config v0.0.0-20210125150955-458a2ee55113 // indirect
	go.mongodb.org/mongo-driver v1.4.5 // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0
)
