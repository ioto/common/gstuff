package constant

const Charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

var MediaTypes = map[string]string{
	"image/jpeg":       "jpg",
	"image/jpg":        "jpg",
	"image/gif":        "gif",
	"image/png":        "png",
	"image/webp":       "webp",
	"video/mp4":        "mp4",
	"video/x-m4v":      "m4v",
	"video/x-matroska": "mkv",
	"video/webm":       "webm",
	"video/quicktime":  "mov",
	"video/x-msvideo":  "avi",
	"video/x-ms-wmv":   "wmv",
	"video/mpeg":       "mpg",
	"video/x-flv":      "flv",
	"video/3gpp":       "3gp",
}
