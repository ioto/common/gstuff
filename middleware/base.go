package middleware

import (
	"io"
	"net/http"

	"gitlab.com/ioto/common/config"
)

var cfg = config.GetConfig()

type bodyDumpResponseWriter struct {
	io.Writer
	http.ResponseWriter
}

func (w *bodyDumpResponseWriter) Write(b []byte) (int, error) {
	return w.Writer.Write(b)
}
